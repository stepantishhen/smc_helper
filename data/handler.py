import re

messages = ['Заказано, нет подтверждения от поставщика']


def comment_handler(code: str):
    d = {}
    code = code.replace('(', '')
    code = code.replace(')', '')
    if '; ' in code:
        code = code.replace('; ', ';')
    elif ', ' in code:
        code = code.replace(', ', ',')
    if ' - ' in code:
        code = code.replace(' - ', '-')
    code = code.rstrip(';')
    if len(re.findall(r'\d{1,2}\.\d{1,2}\.\d{4}', code)) > 1:
        pass
    else:
        if 'Нет подтверждения' in code:
            return messages[0]
        else:
            date, code = code.split()
            if ';' in code:
                code = code.split(';')
            elif ',' in code:
                code = code.split(',')
            d[date] = {}
            for i in code:
                if i.isalnum():
                    d[date]['номер заказа'] = i
                elif i == 'S' or i == 'A' or i == 'R' or i == 'T':
                    d[date]['способ доставки'] = i
                elif len(i) == 2:
                    d[date]['страна поставщик'] = i
                elif 'q' in i:
                    d[date]['готовое количество к отгрузке'] = i[1:]
            return d


tests = ['03.02.2026 (Нет подтверждения);',
         '15.03.2021 (A211KR; KR);',
         '24.02.2021 (T, B35107JP, Be, q0);',
         '05.02.2021 (!!, R, B33708JP-JP, Jp, q6);',
         '02.08.3331 (66/66/66, A, B35076JP-JP, Jp, q0);',
         '02.04.2021 (S, CH-232-21);',
         '22.03.2021 (R, JP-29-36-37-38-21);',
         '10.02.2021 (EW-41-21);',
         '22.03.2021 (A3744Fl; Fl - 14 шт.; 24.02.2021 (Fl-02-21) - 36 шт.);',
         '12.02.2021 (B34827RU; RU); 05.02.2021 (B34672RU; RU);'
         ]

for i in tests:
    print(comment_handler(i))
# print(comment_handler(tests[0]))