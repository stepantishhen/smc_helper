from flask import Flask, render_template, redirect, abort, request
from data.comments import Comment
from data.handler import comment_handler
from data import db_session
from flask_ngrok import run_with_ngrok

app = Flask(__name__)
app.config['SECRET_KEY'] = 'smc_helper_app'


@app.route('/')
def index():
    return render_template('index.html', title='Главная')


@app.route('/first', methods=['GET', 'POST'])
def first():
    db_sess = db_session.create_session()
    if request.method == 'POST':
        add_comment(db_sess, request.form.get('comment'))
        return redirect('/first')
    return render_template('first.html', title='CommentReader',
                           lst=db_sess.query(Comment).order_by(Comment.created_date.desc()))


@app.route('/add_comment', methods=['GET', 'POST'])
def add_comment(session, string):
    comment = Comment(comment=string)
    session.add(comment)
    session.commit()


@app.route('/clear_comments', methods=['GET', 'POST'])
def clear_comments():
    db_sess = db_session.create_session()
    db_sess.query(Comment).delete()
    db_sess.commit()
    return redirect('/first')


@app.route('/second')
def second():
    return render_template('second.html', title='Второе')


@app.route('/third')
def third():
    return render_template('third.html', title='Третье')


if __name__ == '__main__':
    db_session.global_init('db/comments.sqlite')
    # app.run(port=8080, host='127.0.0.1', debug=True)
    app.run(debug=True)